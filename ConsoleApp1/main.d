module main;
import std.stdio;
import std.file;
import std.path;

/**
* This function lists files in a directory(@param path) (recursive) of a certain type(@param type)
* @param path the path to search the files inside.
* @param type the type of file to search.
* @return a list of files that is of the requested type inside the path.
*/
string[] listDir(string path, string type) {
	//Local imports.
    import std.algorithm;
    import std.array;
	//std.file.dirEnteries(string path,SopanMode mode).
	//Search foo all files in this path(recurse), filter by (not a folder, has the same extension) 
	return dirEntries(path, SpanMode.depth)
		//std.algorithm.iteration.filter!(predicate)(T range)
		//Filter by is not folder and endsWith extension.
        .filter!(a => a.isFile && a.name.endsWith(type))
		//std.algorithm.iteration.map(predicate pred)(T range).
		//Map the iterator from dirEntry to a string.
        .map!(a => a.name)
		//To array.
		//std.array.array(T Range)
		.array;
}

/**
* The entry point
*/
int main(string[] args) {
	//Local imports.
	import std.array;
	import std.json;
	
	//List the files inside the "./data" that are txt(recursive).
	string[] files = listDir(std.file.getcwd() ~ dirSeparator ~ "data", ".txt");
	//Interate over all files.
	foreach(string path; files) {
		//Read the file and store in b.
		string b = readText(path);
		//Parse the json in b and store in val.
		JSONValue val = parseJSON(b);
		//Get the directory storing the txt file.
		string dir = dirName(path) ~ dirSeparator;
		//The name of the image file file.
		string name = baseName(path).replace("_json.txt", ".jpg");
		//The path of the image file.
		string imgFile = dir ~ name;
		//The directory to store the output of the program.
		// /results/number(value of spread)/
		dir = dir.replace("data", "results") ~ val.object["spread"].str ~ dirSeparator;
		//The output file's path
		string output = dir ~ name;
		//If the image to put in the output file does not exist go to the next json.
		//std.file.exists(string file)
		if(!imgFile.exists) continue;
		//If the output folder doesn't exist, create one.
		//std.file.exists(string file), std.file.mkdirRecurse(string dir)
		if(!dir.exists) dir.mkdirRecurse;
		//Copy the file to the output folder.
		imgFile.copy(output);
	}
    return 0;
}

